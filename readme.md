# **README DO PROJETO FINAL DA DISCIPLINA DE PROJETO DE SOFTWARE**

## **Alunos:**
### Débora Emili Costa Oliveira (Email: emilli.costa@hotmail.com)
### Igor Augusto Brandão (Email: igorabrandao@gmail.com)
### José gameleira do Rêgo Neto (Email: jgdrneto@gmail.com)


## **Professor:**
### Uirá Kulesza


## **Descrição:**

Esse é o projeto final da disciplina de projeto de software no qual consiste em criar uma aplicação Java para o propósito de interprear, análisar e criar indices estatístios dos dados oferecidos pelo governo federal brasileiro que tenham o tema acidentes de trabalho.


### **Requisitos de execução do extrator**

* Ter o Java JRE instalado no computador.
* Ter o eclipse Neon instalado