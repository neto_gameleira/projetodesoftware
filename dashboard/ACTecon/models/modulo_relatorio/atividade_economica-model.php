<?php

	class AtividadeEconomicaModel extends MainModel
	{
		/**
		 * Class constructor
		 *
		 * Set the database, controller, parameter and user data.
		 *
		 * @since 0.1
		 * @access public
		 * @param object $db PDO Conexion object
		 * @param object $controller Controller object
		*/
		public function __construct( $db = false, $controller = null )
		{
			// Set DB (PDO)
			$this->db = $db;

			// Set the controller
			$this->controller = $controller;

			// Set the main parameters
			$this->parametros = $this->controller->parametros;

			// Set user data
			$this->userdata = $this->controller->userdata;
			
			// Define the active tab
			$GLOBALS['ACTIVE_TAB'] = "Atividade_Economica";
		}

		/**
		 * Get user type list
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_economic_activity_group_list() 
		{
			// Select the necessary data from DB
			$query = $this->db->query('SELECT DISTINCT `GRUPOCNAE` FROM `CNAE` WHERE `DATA_FECHA` IS NULL ORDER BY `GRUPOCNAE`');

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchAll();
		} // get_user_type_list

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_economic_activity_list() 
		{
			// Select the necessary data from DB
			$sql = "SELECT DISTINCT  `DESCRICAO` FROM  `CNAE` WHERE  `DATA_FECHA` IS NULL ORDER BY `DESCRICAO`";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( $query )
				return $query->fetchAll();
			else
				return 0;
		} // get_economic_activity_list

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_economic_year_list() 
		{
			// Select the necessary data from DB
			$sql = "SELECT DISTINCT  `ANO` FROM  `ACT_ATIVIDADE_ECONOMICA` WHERE  `DATA_FECHA` IS NULL ";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( $query )
				return $query->fetchAll();
			else
				return 0;
		} // get_economic_year_list

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function economic_activity_overview( $cnae_ )
		{
			// Select the necessary data from DB
			$sql = "SELECT CNAE.`CNAE`, CNAE.`DESCRICAO`, ATE.`ANO`, SUM(ATE.`QTD_ACIDENTES`) QTD_ACIDENTES
				FROM `ACT_ATIVIDADE_ECONOMICA` AS ATE
				INNER JOIN `CNAE` AS CNAE ON CNAE.`ID_CNAE` = ATE.`ID_CNAE`
				WHERE CNAE.`DESCRICAO` LIKE '%" . $cnae_ . "%' AND ATE.`DATA_FECHA` IS NULL
				GROUP BY CNAE.`DESCRICAO`, ATE.`ANO`";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( $query )
				return $query->fetchAll();
			else
				return 0;
		} // economic_activity_overview

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function economic_activity_overview_by_group( $grupocnae_ )
		{
			// Select the necessary data from DB
			$sql = "SELECT CNAE.`GRUPOCNAE`, CNAE.`DESCRICAO`, ATE.`ANO`, SUM(ATE.`QTD_ACIDENTES`) QTD_ACIDENTES
				FROM `ACT_ATIVIDADE_ECONOMICA` AS ATE
				INNER JOIN `CNAE` AS CNAE ON CNAE.`ID_CNAE` = ATE.`ID_CNAE`
				WHERE CNAE.`GRUPOCNAE` LIKE '%" . $grupocnae_ . "%' AND ATE.`DATA_FECHA` IS NULL
				GROUP BY CNAE.`DESCRICAO`, ATE.`ANO`";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( $query )
				return $query->fetchAll();
			else
				return 0;
		} // economic_activity_overview
	}

?>