<?php

	class VisaoGeralModel extends MainModel
	{
		/**
		 * Class constructor
		 *
		 * Set the database, controller, parameter and user data.
		 *
		 * @since 0.1
		 * @access public
		 * @param object $db PDO Conexion object
		 * @param object $controller Controller object
		*/
		public function __construct( $db = false, $controller = null )
		{
			// Set DB (PDO)
			$this->db = $db;

			// Set the controller
			$this->controller = $controller;

			// Set the main parameters
			$this->parametros = $this->controller->parametros;

			// Set user data
			$this->userdata = $this->controller->userdata;
			
			// Define the active tab
			$GLOBALS['ACTIVE_TAB'] = "Visao_Geral";
		}

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_qtd_accident_by_year( $year_ ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT SUM(`QTD_ACIDENTES`) AS QTD FROM `ACT_MES` WHERE `ANO` = " . $year_ . " AND `DATA_FECHA` IS NULL";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_qtd_accident_by_year

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_qtd_accident_by_month( $month_year_ ) 
		{
			// Select the necessary data from DB
			$sql = "SELECT SUM(`QTD_ACIDENTES`) AS QTD FROM `ACT_MES` WHERE `MES` LIKE '%" . $month_year_ . "%' AND `DATA_FECHA` IS NULL";

			// Execute the sql statement
			$query = $this->db->query($sql);

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchColumn(0);
		} // get_qtd_accident_by_month
	}

?>