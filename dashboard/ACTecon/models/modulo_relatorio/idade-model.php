<?php

	class IdadeModel extends MainModel
	{
		/**
		 * Class constructor
		 *
		 * Set the database, controller, parameter and user data.
		 *
		 * @since 0.1
		 * @access public
		 * @param object $db PDO Conexion object
		 * @param object $controller Controller object
		*/
		public function __construct( $db = false, $controller = null )
		{
			// Set DB (PDO)
			$this->db = $db;

			// Set the controller
			$this->controller = $controller;

			// Set the main parameters
			$this->parametros = $this->controller->parametros;

			// Set user data
			$this->userdata = $this->controller->userdata;
			
			// Define the active tab
			$GLOBALS['ACTIVE_TAB'] = "Idade";
		}

		/**
		 * Get user type list
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_motivo_situacao_list() 
		{
			// Select the necessary data from DB
			$query = $this->db->query('SELECT DISTINCT `DESCRICAO` FROM `MOTIVO_SITUACAO` WHERE `DATA_FECHA` IS NULL ORDER BY `DESCRICAO`');

			// Check if query worked
			if ( ! $query )
				return array();

			// Return data to view
			return $query->fetchAll();
		} // get_user_type_list

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_idade_list() 
		{
			// Select the necessary data from DB
			$sql = "SELECT DISTINCT  `IDADE` FROM  `ACT_IDADE` WHERE  `DATA_FECHA` IS NULL ORDER BY `IDADE`";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( $query )
				return $query->fetchAll();
			else
				return 0;
		} // get_economic_activity_list

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function get_economic_year_list() 
		{
			// Select the necessary data from DB
			$sql = "SELECT DISTINCT  `ANO` FROM  `ACT_IDADE` WHERE  `DATA_FECHA` IS NULL ";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( $query )
				return $query->fetchAll();
			else
				return 0;
		} // get_economic_year_list

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function economic_activity_overview( $idade_ )
		{
			// Select the necessary data from DB
			$sql = "SELECT MS.`DESCRICAO`, AI.`ANO`, SUM(AI.`QTD_ACIDENTES`) QTD_ACIDENTES
				FROM `ACT_IDADE` AS AI
				INNER JOIN `MOTIVO_SITUACAO` AS MS ON MS.`ID_MOTIVO_SITUACAO` = AI.`ID_MOTIVO_SITUACAO`
				WHERE AI.`IDADE` LIKE '%" . $idade_ . "%' AND AI.`DATA_FECHA` IS NULL
				GROUP BY MS.`DESCRICAO`, AI.`ANO`";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( $query )
				return $query->fetchAll();
			else
				return 0;
		} // economic_activity_overview

		/**
		 * Get the site's statistcs
		 * 
		 * @since 0.1
		 * @access public
		*/
		public function economic_activity_overview_by_group( $descricao_ )
		{
			// Select the necessary data from DB
			$sql = "SELECT AI.`IDADE`, AI.`ANO`, SUM(AI.`QTD_ACIDENTES`) QTD_ACIDENTES
				FROM `ACT_IDADE` AS AI
				INNER JOIN `MOTIVO_SITUACAO` AS MS ON MS.`ID_MOTIVO_SITUACAO` = AI.`ID_MOTIVO_SITUACAO`
				WHERE MS.`DESCRICAO` LIKE '%" . $descricao_ . "%' AND AI.`DATA_FECHA` IS NULL
				GROUP BY MS.`DESCRICAO`, AI.`ANO`";

			// Execute the query
			$query = $this->db->query($sql);

			// Check if query worked
			if ( $query )
				return $query->fetchAll();
			else
				return 0;
		} // economic_activity_overview
	}

?>