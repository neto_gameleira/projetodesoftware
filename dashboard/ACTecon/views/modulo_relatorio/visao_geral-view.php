<!-- Search filter -->
<div class="right-sidebar">

	<form action="">
		<h3>Filtro de busca</h3>
		<div class="block">					
			<div>
				<select class="search">
					<option value="">Filtrar por ano...</option>
					<?php  
						// Auxiliary variables
						$initial_year = 1997;
						$final_year = 2015;

						// Run the total of years
						for ( $i = $initial_year; $i < $final_year; $i++ )
						{
							echo "<option value='" . $i . "'>" . $i . "</option>";
						}
					?>
				</select>
			</div>
		</div>
		
		<!--<h3>Hourly Rate</h3>
		<div class="block">
			<div>
				<div class="left">$10</div>
				<div class="right">$500+</div>
				<div class="clearfix"></div>
				<input data-type="range" data-min=10 data-max=500 data-step=10 data-range="[10,400]" data-pattern="$%n" />
			</div>
		</div>-->
	</form>
	
</div>
<!-- End of search filter -->

<!-- start: CONTENT -->
<section id="content" class="container_12 clearfix with-right-sidebar" data-sort=true>

	<h1 class="grid_12 margin-top no-margin-top-phone" title="Visão geral">Visão geral</h1>

	<!-- Year graphic -->
	<div class="grid_12">
		<div class="box">
		
			<div class="header">
				<h2><img class="icon" src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Quantidade de acidentes entre 1997 - 2014</h2>
			</div>
			
			<div class="content" style="height: 300px;">
				<table class=chart >
					<thead>
						<tr>
							<?php  

								// Auxiliary variables
								$initial_year = 1997;
								$final_year = 2015;

								// Empty header
								echo "<th></th>";

								// Run the total of years
								for ( $i = $initial_year; $i < $final_year; $i++ )
								{
									echo "<th>" . $i . "</th>";
								}

							?>
						</tr>
					</thead>
					<tbody>
						<?php

							echo "<tr>";

							// Run the total of years
							for ( $i = $initial_year; $i < $final_year; $i++ )
							{
								// Open the chart line
								echo "<th>Quantidade de acidentes de trabalho por ano</th>";

								// Print the information in the line
								$count = $modelo->get_qtd_accident_by_year($i);
								echo "<td alt='" . $count . "' title='" . $count . "'>" . $count . "</td>";
							}

							// Close the chart line
							echo "</tr>";

						?>

					</tbody>	
				</table>
			</div><!-- End of .content -->
		</div><!-- End of .box -->
	</div>

	<div class="grid_12">
		<div class="box">
		
			<div class="header">
				<h2><img class="icon" src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/chart-up-color.png">Quantidade de acidentes por mês</h2>
			</div>
			
			<div class="content" style="height: 250px;">

				<?php

					$months = array(
					    'Janeiro',
					    'Fevereiro',
					    'Março',
					    'Abril',
					    'Maio',
					    'Junho',
					    'Julho',
					    'Agosto',
					    'Setembro',
					    'Outubro',
					    'Novembro',
					    'Dezembro'
					);

				?>

				<table class=chart data-type=bars>
					<thead>
						<tr>
							<th></th>
							<?php
								foreach ($months as $value) {
								    echo "<th>" . $value . "</th>";
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php

							echo "<tr>";

							// Run the total of years
							foreach ( $months as $value )
							{
								// Open the chart line
								echo "<th>Quantidade de acidentes de trabalho por mês</th>";

								// Print the information in the line
								$count = $modelo->get_qtd_accident_by_month($value);
								echo "<td alt='" . $count . "' title='" . $count . "'>" . $count . "</td>";
							}

							// Close the chart line
							echo "</tr>";

						?>
					</tbody>	
				</table>
			</div><!-- End of .content -->
			
		</div><!-- End of .box -->
	</div><!-- End of .grid_6 -->
</section><!-- End of #content -->