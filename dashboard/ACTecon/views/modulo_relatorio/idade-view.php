<!-- Search filter -->
<div class="right-sidebar">

	<form action="">
		<h3>Filtro de busca</h3>
		<div class="block">					
			<div>
				<select name="MOTIVO_SITUACAO" id="MOTIVO_SITUACAO" class="search" onchange="redirecionar(this.options[this.selectedIndex].value);">
					<option value="">Filtrar por grupo...</option>
					<?php

						// Belt's list
						$list = $modelo->get_motivo_situacao_list();

						foreach ($list as $value)
						{
							echo "<option value='" . HOME_URI . "/modulo_relatorio/idade?MOTIVO_SITUACAO=" . $value[0] . "'>" . $value[0] . "</option>";
						}

					?>
				</select>
			</div>

			<div>
				<select name="IDADE" id="CNAE" class="search" onchange="redirecionar(this.options[this.selectedIndex].value);">
					<option value="">Filtrar por atividade...</option>
					<?php

						// Belt's list
						$list = $modelo->get_idade_list();

						foreach ($list as $value)
						{
							echo "<option value='" . HOME_URI . "/modulo_relatorio/idade?IDADE=" . $value[0] . "'>" . $value[0] . "</option>";
						}

					?>
				</select>
			</div>
		</div>
		
		<!--<h3>Hourly Rate</h3>
		<div class="block">
			<div>
				<div class="left">$10</div>
				<div class="right">$500+</div>
				<div class="clearfix"></div>
				<input data-type="range" data-min=10 data-max=500 data-step=10 data-range="[10,400]" data-pattern="$%n" />
			</div>
		</div>-->
	</form>
	
</div>
<!-- End of search filter -->

<!-- start: CONTENT -->
<section id="content" class="container_12 clearfix with-right-sidebar" data-sort=true>

	<h1 class="grid_12 margin-top no-margin-top-phone" title="Gestão de Usuários">Relatório por atividade econômica</h1>

	<?php

		// Data variable
		$data = null;

		// Título
		$title = "Acidentes por idade";

		// Check if a group or activite was defined
		if (isset($_GET['IDADE']) && $_GET['IDADE'] != '')
		{
			// Get the service ID from GET
			$IDADE_id = $_GET['IDADE'];

			// Load the information (overview)
			$data = $modelo->economic_activity_overview($IDADE_id);

			$title = "Acidentes por Idade (" . $IDADE_id . ")";
		}
		else if (isset($_GET['MOTIVO_SITUACAO']) && $_GET['MOTIVO_SITUACAO'] != '')
		{
			// Get the service ID from GET
			$MOTIVO_SITUACAO = $_GET['MOTIVO_SITUACAO'];

			// Load the information (overview)
			$data = $modelo->economic_activity_overview_by_group($MOTIVO_SITUACAO);

			$title = "Acidentes por idade (" . $MOTIVO_SITUACAO . ")";
		}

	?>

	<div class="grid_12">
		<div class="box">
		
			<div class="header">
				<h2><img class="icon" src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/chart-up-color.png"><?php echo $title; ?></h2>
			</div>
			
			<div class="content" style="height: 500px;">
				<table class=chart >
					<thead>
						<tr>
							<th></th>
							<th>2006</th>
							<th>2007</th>
							<th>2008</th>
							<th>2009</th>
							<th>2010</th>
							<th>2011</th>
							<th>2012</th>
							<th>2013</th>
							<th>2014</th>
						</tr>
					</thead>
					<tbody>
						<?php

							echo "<tr>";

							$description = $data[0]["DESCRICAO"];
							$header_printed = 0;

							// Run through the data
							foreach ( $data as $value ) {
								
								if ( strcmp($description, $value["DESCRICAO"]) != 0 )
								{
									echo "</tr><tr>";
									$header_printed = 0;
								}

								// Open the chart line
								if ( $header_printed == 0 )
								{
									echo "<th>" . $value["DESCRICAO"] . "</th>";
									$header_printed = 1;
								}

								// Print the information in the line
								echo "<td alt='" . $value["QTD_ACIDENTES"] . "' title='" . $value["QTD_ACIDENTES"] . "'>" . $value["QTD_ACIDENTES"] . "</td>";

								$description = $value																																																																																		["DESCRICAO"];

							}

							// Close the chart line
							echo "</tr>";

						?>
					</tbody>	
				</table>
			</div><!-- End of .content -->
			
		</div><!-- End of .box -->
	</div><!-- End of .grid_12 -->
</section><!-- End of #content -->

<script type="text/javascript">
	
	function redirecionar( url_ )
	{
		window.location.href = url_;
	}

</script>