<!-- start: SIDEBAR -->
	<aside>
		<div class="top">

			<!-- Navigation -->
			<nav><ul class="collapsible accordion">

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'Dashboard' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI)); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/dashboard.png" title="Página inicial" alt="Página inicial" height=16 width=16>
						Página Inicial
					</a>
				</li>

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'Usuario' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI, 'modulo_usuario')); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/users.png" title="Usuários" alt="Usuários" height=16 width=16>
						Usuários
					</a>
				</li>

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'Visao_Geral' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI, 'modulo_relatorio/visao_geral')); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/document-task.png" title="Atividade econômica" alt="Visão Geral" height=16 width=16>
						Visão Geral
					</a>
				</li>

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'Atividade_Economica' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI, 'modulo_relatorio/atividade_economica')); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/document-task.png" title="Atividade econômica" alt="Atividade econômica" height=16 width=16>
						Atividade econômica
					</a>
				</li>

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'CBO' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI, 'modulo_relatorio/cbo')); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/document-task.png" title="Classificação Brasileira de Ocupação" alt="Classificação Brasileira de Ocupação" height=16 width=16>
						CBO
					</a>
				</li>

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'Parte_Corpo' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI, 'modulo_relatorio/parte_corpo')); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/document-task.png" title="Parte do corpo" alt="Parte do corpo" height=16 width=16>
						Parte do corpo
					</a>
				</li>

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'UF' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI, 'modulo_relatorio/uf')); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/document-task.png" title="Estado (UF)" alt="Estado (UF)" height=16 width=16>
						Estado (UF)
					</a>
				</li>

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'CID' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI, 'modulo_relatorio/cid')); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/document-task.png" title="CID" alt="CID" height=16 width=16>
						CID
					</a>
				</li>

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'Demografia' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI, 'modulo_relatorio/demografia')); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/document-task.png" title="Informações demográficas" alt="Informações demográficas" height=16 width=16>
						Informações demográficas
					</a>
				</li>

				<li <?php if ( $GLOBALS['ACTIVE_TAB'] == 'Projecao' ) echo "class='current'"; ?> >
					<a href="<?php echo join(DIRECTORY_SEPARATOR, array(HOME_URI, 'modulo_relatorio/demografia')); ?>">
						<img src="<?php echo HOME_URI;?>/assets/img/icons/packs/fugue/16x16/document-task.png" title="Projeções" alt="Projeções" height=16 width=16>
						Projeções
					</a>
				</li>
				
			</ul></nav><!-- End of nav -->				
		</div><!-- End of .top -->
		
		<!--<div class="bottom sticky">
			<div class="divider"></div>
			<div class="progress">
				<div class="bar" data-title="Space" data-value="1285" data-max="5120" data-format="0,0 MB"></div>
				<div class="bar" data-title="Traffic" data-value="8.61" data-max="14" data-format="0.00 GB"></div>
				<div class="bar" data-title="Budget" data-value="20000" data-max="20000" data-format="$0,0"></div>
			</div>
			<div class="divider"></div>
			<div class="buttons">
				<a href="javascript:void(0);" class="button grey open-add-client-dialog">Add New Client</a>
				<a href="javascript:void(0);" class="button grey open-add-client-dialog">Open a Ticket</a>
			</div>
		</div>--><!-- End of .bottom -->
	</aside>
<!-- end: SIDEBAR -->