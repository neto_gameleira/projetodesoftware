<?php

	/**
	 * Modulo_Relatorio - Controller modulo_Relatorio
	 *
	 * @package KMSMVC
	 * @since 0.1
	*/
	class ModuloRelatorioController extends MainController
	{
		/** 
		 * Attributes
		*/
		private $user_ID;

		/**
		 * Get's and set's
		*/
		private function setUserID( $user_ID_ )
		{
			$this->user_ID = $user_ID_;
		}

		private function getUserID()
		{
			return $this->user_ID;
		}

		/** Functions section
		 * Load the page "http://localhost:2380/KMS/admin/modulo_relatorio/visao_geral"
		*/
		public function visaoGeral( )
		{
			// Page title
			$this->title = 'Relatório - Visão Geral';

			// Function parameter
			$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

			/** Load files from view **/

			// Load model
			$modelo = $this->load_model('modulo_relatorio/visao_geral-model');

			// /views/_includes/header.php
			require ABSPATH . '/views/_includes/header.php';

			// /views/_includes/loading_box.php
			require ABSPATH . '/views/_includes/loading_box.php';

			// /views/_includes/lock_screen.php
			require ABSPATH . '/views/_includes/lock_screen.php';

			// /views/_includes/message_box.php
			require ABSPATH . '/views/_includes/message_box.php';

			// /views/modulo_Relatorio/_breadcrumb_visao_geral.php
			require ABSPATH . '/views/modulo_relatorio/_breadcrumb_visao_geral.php';

			// /views/_includes/navbar.php
			require ABSPATH . '/views/_includes/navbar.php';

			// /views/_includes/logo_menu.php
			require ABSPATH . '/views/_includes/logo_menu.php';

			// /views/_includes/toolbar.php
			require ABSPATH . '/views/_includes/toolbar.php';

			// /views/_includes/sidebar.php
			require ABSPATH . '/views/_includes/sidebar.php';

			// /views/modulo_Relatorio/visao_geral-view.php
			require ABSPATH . '/views/modulo_relatorio/visao_geral-view.php';

			// /views/_includes/footer.php
			require ABSPATH . '/views/_includes/footer.php';
		} // cadastrarRelatorio

		/** Functions section
		 * Load the page "http://localhost:2380/KMS/admin/modulo_relatorio/atividade_economica"
		*/
		public function atividadeEconomica( )
		{
			// Page title
			$this->title = 'Relatório - Atividade Econômica';

			// Function parameter
			$parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();

			/** Load files from view **/

			// Load model
			//Aqui muda em relação da aplicação que estamos usando
			$modelo = $this->load_model('modulo_relatorio/atividade_economica-model');

			// /views/_includes/header.php
			require ABSPATH . '/views/_includes/header.php';

			// /views/_includes/loading_box.php
			require ABSPATH . '/views/_includes/loading_box.php';

			// /views/_includes/lock_screen.php
			require ABSPATH . '/views/_includes/lock_screen.php';

			// /views/_includes/message_box.php
			require ABSPATH . '/views/_includes/message_box.php';
			
			//Aqui muda em relação da aplicação que estamos usando
			// /views/modulo_Relatorio/_breadcrumb_atividade_economica.php
			require ABSPATH . '/views/modulo_relatorio/_breadcrumb_atividade_economica.php';

			// /views/_includes/navbar.php
			require ABSPATH . '/views/_includes/navbar.php';

			// /views/_includes/logo_menu.php
			require ABSPATH . '/views/_includes/logo_menu.php';

			// /views/_includes/toolbar.php
			require ABSPATH . '/views/_includes/toolbar.php';

			// /views/_includes/sidebar.php
			require ABSPATH . '/views/_includes/sidebar.php';

			//Aqui muda em relação da aplicação que estamos usando
			///views/modulo_Relatorio/atividade_economica-view.php
			require ABSPATH . '/views/modulo_relatorio/atividade_economica-view.php';

			// /views/_includes/footer.php
			require ABSPATH . '/views/_includes/footer.php';
		} // cadastrarRelatorio
	}
?>