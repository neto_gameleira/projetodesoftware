package inicio;

import conexao.HibernateUtil;
import dao.DAO;
import interpretador.InterpretadorACT_CBO;
import interpretador.InterpretadorACT_CID;
import interpretador.InterpretadorACT_Idade;
import interpretador.InterpretadorACT_Mes;
import interpretador.InterpretadorACT_ParteCorpo;
import interpretador.InterpretadorACT_UF;
import interpretador.InterpretadorACT_atividade_economica;
import tabelas.ActAtividadeEconomica;
import tabelas.ActCbo;
import tabelas.ActCid;
import tabelas.ActIdade;
import tabelas.ActMes;
import tabelas.ActParteCorpo;
import tabelas.ActUf;

public class Principal {

	public static void main(String[] args){
		
		HibernateUtil.iniciarConexao();
		
		int cont=1;
		
		//
		// Acidentes de trabalho por Atividade econômica
		//
		InterpretadorACT_atividade_economica interpretarAE = new InterpretadorACT_atividade_economica("./src/baseDeDados/acidentesDoTrabalhoPorAtividadeEconomica.xml");
		
		//List<ActAtividadeEconomica> listaObjetosAE = interpretarAE.getListaObjetos();
		
		//Salvando objetos
		
		System.out.println("Salvando os dados do relatório de ACT por Atividade econômica\n");
				
		for(ActAtividadeEconomica ae : interpretarAE.getListaObjetos()){
	
			DAO.ACAO.salvar(ae);

			cont++;
			
			if(cont%1000==0){
				System.out.println("Salvo "+ cont +" dado(s) desse relatório por atividade economica");
			}
		}
		
		cont=1;
		
		//
		//Acidentes de trabalho por idade
		//
		
		InterpretadorACT_Idade interpretarIdade = new InterpretadorACT_Idade("./src/baseDeDados/Acidentes do trabalho por idade.xml");	
		
		//Salvando objetos
		
		System.out.println("\n Salvando os dados do relatório de ACT por Idade\n");
						
		for(ActIdade ai : interpretarIdade.getListaObjetos()){

			DAO.ACAO.salvar(ai);
					
			cont++;
					
			if(cont%1000==0){
				System.out.println("Salvo "+ cont +" dado(s) do relatório de acidentes por idade");
			}
		}		
		
		cont=1;
		
		//
		//Acidentes de trabalho por mês
		//
		
		InterpretadorACT_Mes interpretarMes = new InterpretadorACT_Mes("./src/baseDeDados/Acidentes do trabalho por mês.xml");	
		
		
		//Salvando objetos
		
		System.out.println("\n Salvando os dados do relatório de ACT por mês\n");
						
		for(ActMes am : interpretarMes.getListaObjetos()){
					
			DAO.ACAO.salvar(am);
					
			cont++;
					
			if(cont%1000==0){
				System.out.println("Salvo "+ cont +" dado(s) do relatório de acidentes por mês");
			}
		}
		
		cont=1;
		
		//
		//Acidentes de trabalho por parte do corpo
		//
		
		InterpretadorACT_ParteCorpo interpretarParteCorpo = new InterpretadorACT_ParteCorpo("./src/baseDeDados/Acidentes do Trabalho por parte do corpo atingida.xml");	
		
		//Salvando objetos
		
		System.out.println("\n Salvando os dados do relatório de ACT por parte do corpo atingida\n");
						
		for(ActParteCorpo apc : interpretarParteCorpo.getListaObjetos()){
					
			DAO.ACAO.salvar(apc);
					
			cont++;
					
			if(cont%1000==0){
				System.out.println("Salvo "+ cont +" dado(s) do relatório de acidentes por parte do corpo atingida");
			}
		}
		
		cont=1;
		
		//
		//Acidentes de trabalho por CBO
		//
		
		InterpretadorACT_CBO interpretarCBO = new InterpretadorACT_CBO("./src/baseDeDados/Acidentes do trabalho por CBO.xml");	
		
		//Salvando objetos
		
		System.out.println("\n Salvando os dados do relatório de ACT por CBO\n");
						
		for(ActCbo acbo : interpretarCBO.getListaObjetos()){
					
			DAO.ACAO.salvar(acbo);
					
			cont++;
					
			if(cont%1000==0){
				System.out.println("Salvo "+ cont +" dado(s) do relatório de acidentes por CBO");
			}
		}
		
		cont=1;
		
		//
		//Acidentes de trabalho por CID
		//
		
		InterpretadorACT_CID interpretarCID = new InterpretadorACT_CID("./src/baseDeDados/Acidentes do trabalho por CID.xml");	
		
		//Salvando objetos
		
		System.out.println("\n Salvando os dados do relatório de ACT por CID\n");
						
		for(ActCid acid : interpretarCID.getListaObjetos()){
					
			DAO.ACAO.salvar(acid);
					
			cont++;
					
			if(cont%1000==0){
				System.out.println("Salvo "+ cont +" dado(s) do relatório de acidentes por CID");
			}
		}
		
		cont=1;
		
		//
		//Acidentes de trabalho por UF
		//
		
		InterpretadorACT_UF interpretarUF = new InterpretadorACT_UF("./src/baseDeDados/Acidentes do trabalho por UF.xml");	
		
		//Salvando objetos
		
		System.out.println("\n Salvando os dados do relatório de ACT por UF\n");
						
		for(ActUf auf : interpretarUF.getListaObjetos()){
					
			DAO.ACAO.salvar(auf);
					
			cont++;
					
			if(cont%1000==0){
				System.out.println("Salvo "+ cont +" dado(s) do relatório de acidentes por UF");
			}
		}
		
		//Finalizando conexao;
		
		HibernateUtil.finalizarConexao();
	}	

}
