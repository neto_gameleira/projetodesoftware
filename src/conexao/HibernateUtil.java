package conexao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	
	private static final SessionFactory sessionFactory;
	private static final Session session;
	private static final Transaction transacao;
	
	static {
		
        try {
            sessionFactory = new Configuration().configure("/conexao/hibernate.cfg.xml").buildSessionFactory();
            
            session = sessionFactory.openSession();
            
            transacao = session.beginTransaction();
            
        } catch (Throwable ex) {

            System.err.println("Erro na criação da Session." + ex);
            
            throw new ExceptionInInitializerError(ex);
        }
    }
	
	//Representar o inicio da sessão.
	public static void iniciarConexao(){
	}
	
    public static Session getSession() {
        return session;
    }
    
    public static void finalizarConexao(){
    	transacao.commit();
    	session.close();
    	sessionFactory.close();
    }
}
