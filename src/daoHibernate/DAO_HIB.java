package daoHibernate;

import java.util.ArrayList;
import java.util.List;

import conexao.HibernateUtil;
import dao.DAO;

public class DAO_HIB extends DAO {
	
	@Override
	public <T> void salvar(T objeto) {
		HibernateUtil.getSession().save(objeto);
	}

	@Override
	public <T> void deletar(T objeto) {
		HibernateUtil.getSession().update(objeto);
	}

	@Override
	public <T> void atualizar(T objeto) {
		HibernateUtil.getSession().update(objeto);
	}

	@Override
	public <T> List<T> lista(Class<?> ClasseDaTabela) {
		return new ArrayList<T>(HibernateUtil.getSession().createQuery("FROM "+ ClasseDaTabela.getSimpleName()).getResultList());
	}
	
}
