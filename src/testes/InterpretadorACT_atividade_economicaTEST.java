package testes;

import interpretador.InterpretadorACT_atividade_economica;
import junit.framework.TestCase;
import tabelas.ActAtividadeEconomica;
import tabelas.Cnae;
import tabelas.MotivoSituacao;
import tabelas.Situacao;

public class InterpretadorACT_atividade_economicaTEST extends TestCase {
	
	private String nome="./src/baseDeDados/acidentesDoTrabalhoPorAtividadeEconomica.xml";
	
	private InterpretadorACT_atividade_economica interpretar = new InterpretadorACT_atividade_economica(nome);
	
	public void testandoQuantidadeDeObjetos(){
		assertEquals(interpretar.getListaObjetos().size(),23385);
	}
	
	public void testandoCNAE(){
		
		Cnae cnae = new Cnae(9999, "Ignorado");	
		
		assertEquals(interpretar.getListaObjetos().get(0).getCnae().getDescricao(), cnae.getDescricao());
		assertEquals(interpretar.getListaObjetos().get(0).getCnae().getCnae(),cnae.getCnae());
	}
	
	public void testandoMotivo(){
		
		MotivoSituacao motivo = new MotivoSituacao("Típico-Com Cat");	
								
		assertEquals(interpretar.getListaObjetos().get(0).getMotivoSituacao().getDescricao(), motivo.getDescricao());		
		
	}
	
	public void testandoSituacao(){
		
		Situacao situacao = new Situacao("Com Cat Registrada");
								
		assertEquals(interpretar.getListaObjetos().get(0).getSituacao().getDescricao(), situacao.getDescricao());	
		
	}
	
	public void testandoGrupoCNAE(){
		
		String grupoCNAE = "IGNORADO";
								
		assertEquals(interpretar.getListaObjetos().get(0).getCnae().getGrupocnae(), grupoCNAE);	
		
	}
	
	public void testandoNullPoint(){
		int i=0;
		
		for(ActAtividadeEconomica act : interpretar.getListaObjetos()){
			if(act==null){
				i++;
			}	
		}
		
		assertEquals(0, i);
		
	}
	
}
