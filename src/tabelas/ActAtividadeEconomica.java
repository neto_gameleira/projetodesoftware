package tabelas;
// Generated 22/09/2016 21:40:25 by Hibernate Tools 5.1.0.Beta1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * ActAtividadeEconomica generated by hbm2java
 */
@Entity
@Table(name = "ACT_ATIVIDADE_ECONOMICA", catalog = "BD_ACT")
public class ActAtividadeEconomica implements java.io.Serializable {

	private Integer idAtividadeEconomica;
	private Cnae cnae;
	private MotivoSituacao motivoSituacao;
	private Situacao situacao;
	private Integer ano;
	private Integer qtdAcidentes;
	private String dataFecha;

	public ActAtividadeEconomica() {
	}

	public ActAtividadeEconomica(Cnae cnae) {
		this.cnae = cnae;
	}

	public ActAtividadeEconomica(Cnae cnae, MotivoSituacao motivoSituacao, Situacao situacao, Integer ano,
			Integer qtdAcidentes, String dataFecha) {
		this.cnae = cnae;
		this.motivoSituacao = motivoSituacao;
		this.situacao = situacao;
		this.ano = ano;
		this.qtdAcidentes = qtdAcidentes;
		this.dataFecha = dataFecha;
	}
	
	public ActAtividadeEconomica(Cnae cnae, MotivoSituacao motivoSituacao, Situacao situacao, Integer ano,
			Integer qtdAcidentes) {
		this.cnae = cnae;
		this.motivoSituacao = motivoSituacao;
		this.situacao = situacao;
		this.ano = ano;
		this.qtdAcidentes = qtdAcidentes;
	}

	
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID_ATIVIDADE_ECONOMICA", unique = true, nullable = false)
	public Integer getIdAtividadeEconomica() {
		return this.idAtividadeEconomica;
	}

	public void setIdAtividadeEconomica(Integer idAtividadeEconomica) {
		this.idAtividadeEconomica = idAtividadeEconomica;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "ID_CNAE", nullable = false)
	public Cnae getCnae() {
		return this.cnae;
	}

	public void setCnae(Cnae cnae) {
		this.cnae = cnae;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "ID_MOTIVO_SITUACAO")
	public MotivoSituacao getMotivoSituacao() {
		return this.motivoSituacao;
	}

	public void setMotivoSituacao(MotivoSituacao motivoSituacao) {
		this.motivoSituacao = motivoSituacao;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "ID_SITUACAO")
	public Situacao getSituacao() {
		return this.situacao;
	}

	public void setSituacao(Situacao situacao) {
		this.situacao = situacao;
	}

	@Column(name = "ANO")
	public Integer getAno() {
		return this.ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	@Column(name = "QTD_ACIDENTES")
	public Integer getQtdAcidentes() {
		return this.qtdAcidentes;
	}

	public void setQtdAcidentes(Integer qtdAcidentes) {
		this.qtdAcidentes = qtdAcidentes;
	}

	@Column(name = "DATA_FECHA", length = 20)
	public String getDataFecha() {
		return this.dataFecha;
	}

	public void setDataFecha(String dataFecha) {
		this.dataFecha = dataFecha;
	}

}
