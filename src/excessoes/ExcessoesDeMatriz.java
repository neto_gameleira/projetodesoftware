package excessoes;

public abstract class ExcessoesDeMatriz extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public void finalizar(){
		System.exit(-1);
		System.out.println(this.getMessage());
	}
	
	public static class LinOuColMenorQueUmExcessao extends ExcessoesDeMatriz{

		private static final long serialVersionUID = 5693465948705143015L;

		public LinOuColMenorQueUmExcessao() {
			System.out.println("Indices passados por parâmetro são menores ou iguais a zero");
			
			finalizar();
		}

	}
	
	public static class NumDeColDifNumDeLinExcessao extends ExcessoesDeMatriz{

		private static final long serialVersionUID = 5074006594982080585L;

		public NumDeColDifNumDeLinExcessao() {
			System.out.println("Número de colunas da matriz é diferente do número de linhas da outra matriz ");
			finalizar();			
		}

	}
	public static class IDForaDoIntervaloExcessao extends ExcessoesDeMatriz{
	
		private static final long serialVersionUID = -7960445338322451558L;

		public IDForaDoIntervaloExcessao() {
			System.out.println("Indice fora do intervalo permitido");
			
			finalizar();
		}

	}
	
	public static class TamanhosDiferenteExcessao extends ExcessoesDeMatriz{

		private static final long serialVersionUID = -7963110857739446439L;

		public TamanhosDiferenteExcessao() {
			System.out.println("Matrizes tem tamanhos diferentes");
			
			finalizar();
		}

	}
	
	public static class NaoQuadraticaExcessao extends ExcessoesDeMatriz{

		private static final long serialVersionUID = 1L;
		
		public NaoQuadraticaExcessao() {
			System.out.println("A matriz não é quadrática");
			
			finalizar();
		}

	}
	
	public static class QuantDifElementListELinMatrizExcessao extends ExcessoesDeMatriz{

		private static final long serialVersionUID = 1L;
		
		public QuantDifElementListELinMatrizExcessao() {
			System.out.println("O número de elementos da lista tem tamanho diferente da quantidade de elementos da matriz");
			
			finalizar();
		}

	}
	
	public static class SubMatrizExcessao extends ExcessoesDeMatriz{

		private static final long serialVersionUID = 1L;
		
		public SubMatrizExcessao() {
			System.out.println("Indices de linha e/ou coluna maiores que os indices de colunas da matriz");
			
			finalizar();
		}

	}
}
