package interpretador;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import dao.DAO;
import tabelas.ActCbo;
import tabelas.ActParteCorpo;
import tabelas.Cbo;
import tabelas.MotivoSituacao;
import tabelas.ParteCorpo;
import tabelas.Situacao;


public class InterpretadorACT_CBO extends Interpretador<ActCbo> {
	List<Cbo> results;
		
	/*	Construtor da classe
	 *
	 *	@params String Nome do arquivo que sera obidos os dados
     *	@params List<Integer> Lista com a ordem de atribuicao
	 */
	public InterpretadorACT_CBO(String nomeDoArquivo){
		super(nomeDoArquivo);
		
		results = DAO.ACAO.lista(Cbo.class);
		
		//analise semantica
		analiseSemantica();
	}

	/*	Metodo para realizar a analise semantica das string obtidas no arquivo
	 *
	 * 	@return List<AcidentesPorAtividadeEconomica> Lista de objetos do tipo AcidentesPorAtividadeEconomica
	 */
	public void analiseSemantica(){

		List<ActCbo> acidentes = new ArrayList<ActCbo>();

		for(int i=0;i<this.getValores().size();i++){
			acidentes.add(atribuirValores(this.getValores().get(i)));
		}

		this.setListaObjetos(acidentes);
		
	}

	/*	Metodo que linka os valores nas strings com os valores dos atributos das classe
	 *
	 * 	@params List<String> Lista dos atributos da classe
	 *
	 * 	@return AcidentesPorAtividadeEconomica Objeto com os valores dos atributos valorados com as strings dos arquivo
	 */
	private ActCbo atribuirValores(List<String>objeto){
		
		return  new ActCbo(
				cboCerto(new Cbo(objeto.get(1))),
				motivoSituacaoCerto(new MotivoSituacao(objeto.get(2))),
				situacaoCerto(new Situacao(objeto.get(4))),
				new Integer(objeto.get(3)),
				objeto.get(0));
	}

	private Cbo cboCerto(Cbo cbo) {
	
		for(Cbo c : results){
			if(c.getDescricao().equals(cbo.getDescricao())){
				return c;
			}
		}

		DAO.ACAO.salvar(cbo);
						
		results.add(cbo);
		
		return cbo;
	}

}
