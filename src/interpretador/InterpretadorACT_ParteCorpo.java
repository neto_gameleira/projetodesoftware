package interpretador;

import java.util.ArrayList;
import java.util.List;

import dao.DAO;
import tabelas.ActParteCorpo;
import tabelas.MotivoSituacao;
import tabelas.ParteCorpo;
import tabelas.Situacao;


public class InterpretadorACT_ParteCorpo extends Interpretador<ActParteCorpo> {
	List<ParteCorpo> results;
		
	/*	Construtor da classe
	 *
	 *	@params String Nome do arquivo que sera obidos os dados
     *	@params List<Integer> Lista com a ordem de atribuicao
	 */
	public InterpretadorACT_ParteCorpo(String nomeDoArquivo){
		super(nomeDoArquivo);
		
		results = DAO.ACAO.lista(ParteCorpo.class);
		
		//analise semantica
		analiseSemantica();
	}

	/*	Metodo para realizar a analise semantica das string obtidas no arquivo
	 *
	 * 	@return List<AcidentesPorAtividadeEconomica> Lista de objetos do tipo AcidentesPorAtividadeEconomica
	 */
	public void analiseSemantica(){

		List<ActParteCorpo> acidentes = new ArrayList<ActParteCorpo>();

		for(int i=0;i<this.getValores().size();i++){
			acidentes.add(atribuirValores(this.getValores().get(i)));
		}

		this.setListaObjetos(acidentes);
		
	}

	/*	Metodo que linka os valores nas strings com os valores dos atributos das classe
	 *
	 * 	@params List<String> Lista dos atributos da classe
	 *
	 * 	@return AcidentesPorAtividadeEconomica Objeto com os valores dos atributos valorados com as strings dos arquivo
	 */
	private ActParteCorpo atribuirValores(List<String>objeto){
		
		return  new ActParteCorpo(
				motivoSituacaoCerto(new MotivoSituacao(objeto.get(2))),
				parteCorpoCerto(new ParteCorpo(objeto.get(1))),
				situacaoCerto(new Situacao(objeto.get(4))),
				new Integer(objeto.get(0)),
				new Integer(objeto.get(3))
				);
	}

	private ParteCorpo parteCorpoCerto(ParteCorpo parteCorpo) {
		
		for(ParteCorpo pc : results){
			if(pc.getDescricao().equals(parteCorpo.getDescricao())){
				return pc;
			}
		}

		DAO.ACAO.salvar(parteCorpo);
				
		results.add(parteCorpo);
		
		return parteCorpo;
	}

}
