package interpretador;

import java.util.ArrayList;
import java.util.List;

import tabelas.ActMes;
import tabelas.MotivoSituacao;
import tabelas.Situacao;


public class InterpretadorACT_Mes extends Interpretador<ActMes> {
	
	/*	Construtor da classe
	 *
	 *	@params String Nome do arquivo que sera obidos os dados
     *	@params List<Integer> Lista com a ordem de atribuicao
	 */
	public InterpretadorACT_Mes(String nomeDoArquivo){
		super(nomeDoArquivo);

		//analise semantica
		analiseSemantica();
	}

	/*	Metodo para realizar a analise semantica das string obtidas no arquivo
	 *
	 * 	@return List<AcidentesPorAtividadeEconomica> Lista de objetos do tipo AcidentesPorAtividadeEconomica
	 */
	public void analiseSemantica(){

		List<ActMes> acidentes = new ArrayList<ActMes>();

		for(int i=0;i<this.getValores().size();i++){
			acidentes.add(atribuirValores(this.getValores().get(i)));
		}

		this.setListaObjetos(acidentes);
		
	}

	/*	Metodo que linka os valores nas strings com os valores dos atributos das classe
	 *
	 * 	@params List<String> Lista dos atributos da classe
	 *
	 * 	@return AcidentesPorAtividadeEconomica Objeto com os valores dos atributos valorados com as strings dos arquivo
	 */
	private ActMes atribuirValores(List<String>objeto){
		
		return  new ActMes(
				motivoSituacaoCerto(new MotivoSituacao(objeto.get(2))),
				situacaoCerto(new Situacao(objeto.get(5))),
				new Integer(objeto.get(0)),
				objeto.get(1),		
				new Integer(objeto.get(3)),
				objeto.get(4));
	}

}
