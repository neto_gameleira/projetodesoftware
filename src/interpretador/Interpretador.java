package interpretador;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import dao.DAO;
import tabelas.MotivoSituacao;
import tabelas.Situacao;


public abstract class Interpretador<T>{
	//Atributos
	private List<List<String>> valores;
	private List<T> listaObjetos;

	private List<MotivoSituacao> resultMot;
	private List<Situacao> resultSit;
	/*	Construtor da classe
     *
     *	@params String Nome do arquivo que sera obidos os dados
     */
    public Interpretador(String nomeDoArquivo){
    	
    	resultMot = DAO.ACAO.lista(MotivoSituacao.class);
    	resultSit = DAO.ACAO.lista(Situacao.class);
    	
    	//Dividindo o arquivo em linhas
    	List<String> dados = leitura(nomeDoArquivo);

    	//Dividindo a string em listas de strings para usar como atributos do objeto
    	this.valores = analiseSintatica(dados);

    	//Organiza os atributos a serem inseridos no objeto
    	for(List<String> objeto : valores){
    		objeto = organizar(objeto);
    	}
    	
    	//Criar a conexão
    	
    }

    /*	Metodo que organiza os termos na sequencia dos atributos da classe
    *
    *	@params List<String> Nome do arquivo que sera obidos os dados
    *	@return List<Integer> Lista com a ordem de atribuicao
    */
    public List<String> organizar(List<String> objeto){
    	return objeto;
	}

    /*	Metodo responsavel pela analise sintatica das string que compoe os objetos
     *
     * 	@params List<String> Dados em que serao feito a analise sintatica
     *
     * 	@return List<List<String>> Lista de lista de String com os atributos de cada objeto
     */
    public static List<List<String>> analiseSintatica(List<String> dados){
    	
    	
    	if(dados.isEmpty()){
    		System.out.println("Está vazio");
    	}
    	
    	//Lista de retorno para o matodo
    	List<List<String>> listaRetorno = new ArrayList<>();

    	//Lista com os atributos do objeto
    	List<String> listaObjeto = new ArrayList<>();

    	//Interando sobre todas as linhas
    	for(int i=0;i<dados.size();i++){
    		//Analisa cada variavel

	    	if(dados.get(i).contains("<variavel")){
	    		//Pegando apenas os valores objtidos na string
	    		listaObjeto.add(dados.get(i).substring(dados.get(i).indexOf(">")+1, dados.get(i).lastIndexOf("<")));
	    	}else{
	    		if(dados.get(i).contains("</node>")){
	    			listaRetorno.add(new ArrayList<String>(listaObjeto));
	    			listaObjeto.clear();
	    		}
	    	}

    	}
    	
    	return listaRetorno;
    }

    /*	Método para leitura de arquivo
     *
     * 	@params String nome do arquivo que se deseja abrir
     *
     * 	@return List<String> Lista de String no qual cada linha representa uma String
     */
    public static List<String> leitura(String nomeDoArquivo){
    	 	
    	List<String> linhas = new ArrayList<String>();
    	    	
    	try {
			BufferedReader buffer = new BufferedReader(new InputStreamReader(new FileInputStream(nomeDoArquivo), "ISO-8859-1"));
			
			while(buffer.ready()){
				linhas.add(buffer.readLine());
			}
			
			buffer.close();
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
    	
		if(linhas.isEmpty()){
			System.out.println("Linhas vazias");
		}
		
    	return linhas;
    }

    /*	Metodo obter as strings dos atributos dos objetos
     *
     * 	@return List<List<String>> Lista de lista de String dos atributos de varios objetos
     */
    public List<List<String>> getValores() {
		return valores;
	}

    /*	Metodo para inserir as strings dos atributos dos objetos
     *
     * 	@params List<List<String>> Lista de lista de String dos atributos de varios objetos
     */
	public void setValores(List<List<String>> nValores) {
		this.valores = nValores;
	}

	/*	Metodo para obter a lista de objetos formados
    *
    * 	@return List<T> Lista dos objetos formados pelo interpretador
    */
	public List<T> getListaObjetos() {
		return listaObjetos;
	}

	/*	Metodo para inserir uma nova lista de objetos formados
    *
    * 	@params List<T> Lista dos objetos
    */
	public void setListaObjetos(List<T> nListaObjetos) {
		this.listaObjetos = nListaObjetos;
	}
		
	/*	Metodo para verificar existência de campos
	 *
	 * 	@params String Nome a ser procurado
	 *	@params String Nome do campo no banco de dados
	 *	@params String Tabela a ser procurada no banco de dados
	 *
	 * 	@return Boolean Dizendo se objeto existe ou não existe
	 */
	public boolean verificarExistencia(String nome,String campo, String tabela){
		//return this.getModel().pesquisar(nome, campo, tabela);
		return false;
	} 
	
	public MotivoSituacao motivoSituacaoCerto(MotivoSituacao m){
			
		for(MotivoSituacao motSit : resultMot){
			if(motSit.getDescricao().equals(m.getDescricao())){
				return motSit;
			}
		}

		DAO.ACAO.salvar(m);
		
		resultMot.add(m);
		
		return m;
	}
	
	public Situacao situacaoCerto(Situacao sit){
	
		for(Situacao sitN : resultSit){
			if(sitN.getDescricao().equals(sit.getDescricao())){
				return sitN;
			}
		}

		DAO.ACAO.salvar(sit);
				
		resultSit.add(sit);
		
		return sit;
	}
	
	/*	Metodo abstrato para ser implementado nas classe filhos
     *
     * 	@params List<List<String>>
     */
    public abstract void analiseSemantica();

}
