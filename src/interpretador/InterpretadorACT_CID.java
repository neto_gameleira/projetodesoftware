package interpretador;

import java.util.ArrayList;
import java.util.List;

import dao.DAO;
import tabelas.ActCid;
import tabelas.Cid;
import tabelas.MotivoSituacao;
import tabelas.Situacao;


public class InterpretadorACT_CID extends Interpretador<ActCid> {
	List<Cid> results;
		
	/*	Construtor da classe
	 *
	 *	@params String Nome do arquivo que sera obidos os dados
     *	@params List<Integer> Lista com a ordem de atribuicao
	 */
	public InterpretadorACT_CID(String nomeDoArquivo){
		super(nomeDoArquivo);
		
		results = DAO.ACAO.lista(Cid.class);
		
		//analise semantica
		analiseSemantica();
	}

	/*	Metodo para realizar a analise semantica das string obtidas no arquivo
	 *
	 * 	@return List<AcidentesPorAtividadeEconomica> Lista de objetos do tipo AcidentesPorAtividadeEconomica
	 */
	public void analiseSemantica(){

		List<ActCid> acidentes = new ArrayList<ActCid>();

		for(int i=0;i<this.getValores().size();i++){
			acidentes.add(atribuirValores(this.getValores().get(i)));
		}

		this.setListaObjetos(acidentes);
		
	}

	/*	Metodo que linka os valores nas strings com os valores dos atributos das classe
	 *
	 * 	@params List<String> Lista dos atributos da classe
	 *
	 * 	@return AcidentesPorAtividadeEconomica Objeto com os valores dos atributos valorados com as strings dos arquivo
	 */
	private ActCid atribuirValores(List<String>objeto){
		
		return  new ActCid(
				cidCerto(new Cid(objeto.get(2).substring(4))),
				motivoSituacaoCerto(new MotivoSituacao(objeto.get(1))),
				situacaoCerto(new Situacao(objeto.get(4))),
				new Integer(objeto.get(0)),
				new Integer(objeto.get(3))
				);
	}

	private Cid cidCerto(Cid cid) {
	
		for(Cid c : results){
			if(c.getDescricao().equals(cid.getDescricao())){
				return c;
			}
		}

		DAO.ACAO.salvar(cid);
				
		results.add(cid);
		
		return cid;
	}

}
