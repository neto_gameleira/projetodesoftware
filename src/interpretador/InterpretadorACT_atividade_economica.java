package interpretador;

import java.util.ArrayList;
import java.util.List;

import dao.DAO;
import tabelas.ActAtividadeEconomica;
import tabelas.Cnae;
import tabelas.MotivoSituacao;
import tabelas.Situacao;
import util.Par;

public class InterpretadorACT_atividade_economica extends Interpretador<ActAtividadeEconomica> {
	
	List<Cnae> results;
	
	/*	Construtor da classe
	 *
	 *	@params String Nome do arquivo que sera obidos os dados
     *	@params List<Integer> Lista com a ordem de atribuicao
	 */
	public InterpretadorACT_atividade_economica(String nomeDoArquivo){
		super(nomeDoArquivo);
		
		results = DAO.ACAO.lista(Cnae.class);
		
		//analise semantica
		analiseSemantica();
	}

	/*	Metodo para realizar a analise semantica das string obtidas no arquivo
	 *
	 * 	@return List<AcidentesPorAtividadeEconomica> Lista de objetos do tipo AcidentesPorAtividadeEconomica
	 */
	public void analiseSemantica(){

		List<ActAtividadeEconomica> acidentes = new ArrayList<ActAtividadeEconomica>();

		for(int i=0;i<this.getValores().size();i++){
			acidentes.add(atribuirValores(this.getValores().get(i)));
		}

		this.setListaObjetos(acidentes);
		
	}

	/*	Metodo que linka os valores nas strings com os valores dos atributos das classe
	 *
	 * 	@params List<String> Lista dos atributos da classe
	 *
	 * 	@return AcidentesPorAtividadeEconomica Objeto com os valores dos atributos valorados com as strings dos arquivo
	 */
	private ActAtividadeEconomica atribuirValores(List<String>objeto){
		
		return  new ActAtividadeEconomica(
				cnaeCerto(interpretarCNAE(objeto.get(1),objeto.get(5), objeto.get(6))),
				motivoSituacaoCerto(new MotivoSituacao(objeto.get(2))),
				situacaoCerto(new Situacao(objeto.get(4))),
				new Integer(objeto.get(0)),
				new Integer(objeto.get(3)));
	}

	private Cnae cnaeCerto(Cnae c){

		for(Cnae cnae : results){
			if(cnae.getCnae() == c.getCnae()){
				return cnae;
			}
		}

		DAO.ACAO.salvar(c);
				
		results.add(c);
		
		return c;
	}
	
	/*	Metodo para obter os campos separados do CNAE
	 *
	 * 	@params String Campo representativo do CNAE
	 *
	 * 	@return Par<String,Integer> Par com os valores a serem adicionados nos atributos do objeto da classe CNAE
	 */
	private Cnae interpretarCNAE(String objeto, String cnaeGrupo, String cnaeGrLetra){
		Par<String, Integer> cnaePar;
		
		if(objeto.contains(":")){
			cnaePar = new Par<String, Integer>(objeto.substring(objeto.indexOf(":")+1),new Integer(objeto.substring(0, objeto.indexOf(":"))));
		}else{
			cnaePar = new Par<String,Integer>(objeto,0);
		}
		
		Cnae c = new Cnae(cnaePar.getSegundo(), cnaePar.getPrimeiro(), cnaeGrupo, cnaeGrLetra);
		
		return c;
	}
}
