package interpretador;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import tabelas.ActUf;
import tabelas.MotivoSituacao;


public class InterpretadorACT_UF extends Interpretador<ActUf> {
	
	Session s;
	
	/*	Construtor da classe
	 *
	 *	@params String Nome do arquivo que sera obidos os dados
	 */
	public InterpretadorACT_UF(String nomeDoArquivo){
		super(nomeDoArquivo);

		//analise semantica
		analiseSemantica();
	}

	/*	Metodo para realizar a analise semantica das string obtidas no arquivo
	 *
	 * 	@return List<AcidentesPorAtividadeEconomica> Lista de objetos do tipo AcidentesPorAtividadeEconomica
	 */
	public void analiseSemantica(){

		List<ActUf> acidentes = new ArrayList<ActUf>();

		for(int i=0;i<this.getValores().size();i++){
			acidentes.add(atribuirValores(this.getValores().get(i)));
		}

		this.setListaObjetos(acidentes);
		
	}

	/*	Metodo que linka os valores nas strings com os valores dos atributos das classe
	 *
	 * 	@params List<String> Lista dos atributos da classe
	 *
	 * 	@return AcidentesPorAtividadeEconomica Objeto com os valores dos atributos valorados com as strings dos arquivo
	 */
	private ActUf atribuirValores(List<String>objeto){
		
		return  new ActUf(
				motivoSituacaoCerto(new MotivoSituacao(objeto.get(2))),
				new Integer(objeto.get(0)),
				objeto.get(1),		
				new Integer(objeto.get(3))
				);
	}

}
